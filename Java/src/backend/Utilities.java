package backend;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * This class gets all the needed data from the database
 * @author Johanna Luangxay
 * @version 2020-12-20
 *
 */
public class Utilities {

	Connection con;
	private static DecimalFormat df = new DecimalFormat("0.00");
	ArrayList<Product> productList = new ArrayList<Product>();
	ArrayList<Extra> extraList = new ArrayList<Extra>();
	
	/**
	 * Constructor that gets the DB connection
	 * @param con
	 */
	public Utilities(Connection con)
	{
		this.con = con;
	}
	
	/**
	 * Gets the appropriate query that will get all the price. Then adds them together to return it
	 * @param query 
	 * @param id
	 * @return searchPrice double that represent the value of the amount searched
	 * @throws SQLException
	 */
	public double getNumber(String query, String id) throws SQLException
	{
		String priceQuery = query;
		PreparedStatement pps = con.prepareStatement(priceQuery);
		
		pps.setString(1,id);
		
		ResultSet rpps = pps.executeQuery();
		
		double searchPrice = 0;
		while(rpps.next()) 
		{
			searchPrice = searchPrice+ rpps.getDouble(1);
		}
		return searchPrice;
	}

	/**
	 * Gets the whole order total price
	 * @param shoppingCartID
	 * @return double that represents the total of the order
	 * @throws SQLException
	 */
	public double getOrderTotal(String shoppingCartID) throws SQLException
	{
		String getTotal = "{? = call getOrderTotal(?)}";
		CallableStatement cstmt = con.prepareCall(getTotal);
		cstmt.registerOutParameter(1, Types.FLOAT);
		cstmt.setString(2, shoppingCartID);
		cstmt.execute();
		return Double.parseDouble(df.format(cstmt.getFloat(1)));
	}

	/**
	 * Gets the total for a menu and all its product by calling the database function
	 * @param shoppingCartID
	 * @return
	 * @throws SQLException
	 */
	public int getTotalMenu(String shoppingCartID) throws SQLException
	{
		String getMenu = "{? = call getTotalMenu(?)}";
		CallableStatement cstmt = con.prepareCall(getMenu);
		cstmt.registerOutParameter(1, Types.FLOAT);
		cstmt.setString(2, shoppingCartID);
		cstmt.execute();
		return Math.round(cstmt.getFloat(1));
	}
	
	/**
	 * Gets the next available ID 
	 * @param query appropriate query to count total of different id
	 * @return String representing the next available id
	 * @throws SQLException
	 */
	public String getNewID(String query) throws SQLException
	{
		String newID = "";
		int total = 0;
		
		String totalQuery = query;
		PreparedStatement tps = con.prepareStatement(totalQuery);
		ResultSet rtps = tps.executeQuery();
		
		while(rtps.next()) 
		{
			total= Integer.parseInt((String) rtps.getString(1));
		}

		int ALLOWED_CHAR = 5;
		int NUMBER_ADDED = 1;
		total =total+NUMBER_ADDED;
				
		newID = String.format("%0"+ALLOWED_CHAR+"d", total);
			
		return newID;
	}
	
	/**
	 * Gets the customer ID based on its username
	 * @param username
	 * @return String that represents the customer ID
	 * @throws SQLException
	 */
	public String getCustomerID(String username) throws SQLException
	{
		String getCustomerID = "{? = call getCustomerID(?)}";
		CallableStatement cstmt = con.prepareCall(getCustomerID);
		cstmt.registerOutParameter(1, Types.VARCHAR);
		cstmt.setString(2, username);
		cstmt.execute();
		return cstmt.getString(1);
	}

	/**
	 * Calls the function in the DB that gets the amount of freebies a customer has
	 * @param customerID
	 * @return int reprensenting the amount of bonus allowed
	 * @throws SQLException
	 */
	public int getFreebies(String customerID) throws SQLException
	{
		String freebiesQuery = "{? = call getFreebies(?)}";
		CallableStatement cstmt = con.prepareCall(freebiesQuery);
		cstmt.registerOutParameter(1, Types.INTEGER);
		cstmt.setString(2, customerID);
		cstmt.execute();
		return cstmt.getInt(1);
	}
	
	/**
	 * Prints all the allowed extra for the product
	 * @param product_id
	 * @return int representing the number of extra for the product
	 * @throws SQLException
	 */
	public int getExtras (String product_id) throws SQLException
	{
		extraList.clear();
		String extraQuery = "SELECT extra_id,item_name,price FROM extras "
				+ "JOIN customization USING(extra_id) "
				+ "JOIN products USING(product_id) "
				+ "WHERE product_id LIKE ?";
		PreparedStatement eps = con.prepareStatement(extraQuery);
		eps.setString(1, product_id);
		ResultSet reps = eps.executeQuery();
		
		while(reps.next()) 
		{
			String extra_id = reps.getString(1);
			String extra_name = reps.getString(2);
			double price = reps.getDouble(3);
			//Makes an extra object and adds it to the array list
			Extra current_extra= new Extra(extra_id,extra_name,price);
			extraList.add(current_extra);
			
		}
				
		//Prints the extra if the list is not empty
		if(extraList.size() != 0)
		{
			System.out.println("\nHere are the possible extras for this product: ");
			for(Extra e:extraList)
			{
				System.out.println(e);
			}
			System.out.println("\n");
		}
		return extraList.size();
	}

	/**
	 * Prints all the products that are in the DB
	 * @throws NumberFormatException
	 * @throws SQLException
	 */
	public void getProducts() throws NumberFormatException, SQLException
	{
		productList.clear();
		String productQuery = "SELECT product_id,product_name,retail,quantity FROM products";
		PreparedStatement pps = con.prepareStatement(productQuery);
		ResultSet rpps = pps.executeQuery();
		
		while(rpps.next()) 
		{
			String product_id = rpps.getString(1);
			String productName = rpps.getString(2);
			double retail = rpps.getDouble(3);
			int quantity = rpps.getInt(4);
			//Makes a product object and puts it in the array list
			Product current_product = new Product(product_id,productName,retail,quantity);
			productList.add(current_product);
		}
		//Prints all the products in the DB
		System.out.println("\nHere is CoffeZone menu");
		for(Product p:productList)
		{
			System.out.println(p);
		}
		System.out.println("\n");
	}
	
}

package backend;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * This class adds all data linked to ordering in the database
 * @author Johanna Luangxay
 * @version 2020-12-20
 *
 */
public class Ordering {

	private Connection con;
	private Utilities u;
	private static DecimalFormat df = new DecimalFormat("0.00");

	/**
	 * Constructor that gets the DB connection
	 * @param con
	 */
	public Ordering(Connection con)
	{
		this.con = con;
		u = new Utilities(con);
	}
	
	/**
	 * This methods creates the menu based on the given information
	 * @param menu_id
	 * @param product_id
	 * @param username
	 * @param customID
	 * @param menuQuantity
	 * @throws SQLException
	 */
	public void createMenu(String menu_id, String product_id, String username, String customID, int menuQuantity) throws SQLException
	{
		String customerID = u.getCustomerID(username);
		
		try
		{			
			con.setAutoCommit(false);
			
			String productQuery = "SELECT retail FROM products WHERE product_id LIKE ?";
			String customQuery = "SELECT custom_price FROM custom_quantity WHERE custom_id LIKE ?";
			double menu_price =  Math.round(((u.getNumber(productQuery,product_id)+u.getNumber(customQuery,customID))*menuQuantity) * 100.0) / 100.0;
			System.out.println("\nPrice of the menu: "+menu_price+"$");
			
			String createMenuQuery = "INSERT INTO menu VALUES(?,?,?,?,?,?)";
			PreparedStatement cmps = con.prepareStatement(createMenuQuery);
			
			cmps.setString(1,menu_id);
			cmps.setString(2, product_id);
			cmps.setString(3,customerID);
			cmps.setString(4, customID);
			cmps.setDouble(5, menu_price);
			cmps.setInt(6, menuQuantity);
			int cpRow = cmps.executeUpdate();
			System.out.println(cpRow+" customization was added.");
			con.commit();
		}
		catch(SQLException e)
		{
			int errorCode = e.getErrorCode();
			if(errorCode == 20000)
			{
				//Tells the user the actual quantity in stock
				String quantityQuery = "SELECT quantity FROM products WHERE product_id LIKE ?";
				System.out.println("\nNot enough product to add to the menu. There are currently "+Math.round(u.getNumber(quantityQuery,product_id))+" of this product.\n");
			}
			else
			{
				System.out.println("\nMenu creation failed for the following reason: "+e+"\n");	
			}
			con.rollback();
		}
		
	}
	
	/**
	 * Adds data to the customQuantity table (for users that choose to put extras)
	 * @param customID
	 * @param extra_id
	 * @param quantity
	 * @throws NumberFormatException
	 * @throws SQLException
	 */
	public void createCustomQuantity(String customID, String extra_id, int quantity) throws NumberFormatException, SQLException
	{
		try
		{			
			con.setAutoCommit(false);
			
			String query =  "SELECT price FROM extras WHERE extra_id LIKE ?";
			double custom_price = u.getNumber(query, extra_id)*quantity;
			System.out.println("Price of the customization: "+custom_price+"$");
			
			String customPriceQuery = "INSERT INTO custom_quantity VALUES(?,?,?,?)";
			PreparedStatement cpps = con.prepareStatement(customPriceQuery);
			
			cpps.setString(1,customID);
			cpps.setString(2, extra_id);
			cpps.setInt(3, quantity);
			cpps.setDouble(4, custom_price);

			cpps.executeUpdate();
			System.out.println("The customization was succesfully added.");
			con.commit();
		}
		catch(SQLException e)
		{
			System.out.println("\nCustomization failed for the following reason: "+e+"\n");
			con.rollback();
		}
	}

	/**
	 * Adds data to the shopping cart
	 * @param shoppingCartID
	 * @param menuID
	 * @param quantity
	 * @throws SQLException
	 */
	public void createShoppingCart(String shoppingCartID, String menuID, int quantity) throws SQLException
	{
		try
		{			
			con.setAutoCommit(false);
			
			String menuPriceQuery = "SELECT price FROM menu WHERE menu_ID LIKE ?";
			double menu_price = u.getNumber(menuPriceQuery,menuID)*quantity;
			System.out.println("Price of this item in the shopping cart: "+menu_price+"$");
			
			String shoppingCartQuery = "INSERT INTO shopping_cart VALUES(?,?,?,?)";
			PreparedStatement scps = con.prepareStatement(shoppingCartQuery);
			
			scps.setString(1,shoppingCartID);
			scps.setString(2, menuID);
			scps.setDouble(3, menu_price);
			scps.setInt(4, quantity);

			scps.executeUpdate();
			System.out.println("The menu was succesfully added to your shopping cart.\n");
			con.commit();
		}
		catch(SQLException e)
		{
			System.out.println("\nShopping Cart failed for the following reason: "+e+"\n");
			con.rollback();
		}
	}

	/**
	 * Add data to the shoppingOrder table which links between the shopping cart and order table
	 * @param shoppingCartID
	 * @param orderID
	 * @throws SQLException
	 */
	public void createShoppingOrder(String shoppingCartID, String orderID) throws SQLException
	{
		//Calls the procedure to add the data
		String addShoppingOrder = "{call createShoppingOrder(?,?)}";
		CallableStatement cstmt = con.prepareCall(addShoppingOrder);
		cstmt.setString(1, shoppingCartID);
		cstmt.setString(2, orderID);
		cstmt.execute();
	}
	
	/**
	 * Adds an order but validate 1st if it is in the allowed period of time
	 * @param customerID
	 * @param address
	 * @param shoppingCartID
	 * @throws SQLException
	 */
	public void createOrder(String customerID, String address, String shoppingCartID) throws SQLException
	{
		//Determine current time and format it + the limit of time
		LocalTime currentTime = LocalTime.now(); 
		DateTimeFormatter formatTime = DateTimeFormatter.ofPattern("HH:mm:ss");
		LocalTime beforeTime = LocalTime.parse("06:00:00",formatTime); 
		LocalTime afterTime = LocalTime.parse("22:00:00",formatTime);

		//Allows the order adding only if in the right period of time
		if(currentTime.isAfter(afterTime) || currentTime.isBefore(beforeTime))
		{
			System.out.println("It is currently "+currentTime+". Order are allowed only between 6 am and 10 pm.\n");
		}
		else
		{
			LocalDate date = LocalDate.now(); 
			DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			
			String addCustomer = "{call addOrder(?,?,?,?,?)}";
			CallableStatement cstmt = con.prepareCall(addCustomer);

			String newIDQuery = "SELECT COUNT(*) FROM orders";
			String orderID = u.getNewID(newIDQuery);
			cstmt.setString(1, orderID);
			cstmt.setString(2, customerID);
			cstmt.setInt(3, u.getTotalMenu(shoppingCartID));
			cstmt.setDate(4, java.sql.Date.valueOf(date.format(formatDate)));
			cstmt.setString(5, address);
			cstmt.execute();
			
			//Calls the method that adds the data to the shopping_order table
			createShoppingOrder(shoppingCartID,orderID);
			
			System.out.println("Your order has been placed");
		}
		
	}
	
	/**
	 * If the users chooses to use their bonus, this method remove it's freebies and reduce the amount on the total
	 * @param freebies
	 * @param customerID
	 * @param shoppingCartID
	 * @return total Double that represents the final amount 
	 * @throws SQLException
	 */
	public double useFreebies(int freebies, String customerID,String shoppingCartID) throws SQLException
	{
		//Calls the procedure that updates the customer table and determine final amount
		String addShoppingOrder = "{call useFreebies(?,?,?,?)}";
		CallableStatement cstmt = con.prepareCall(addShoppingOrder);
		
		cstmt.setInt(1, freebies);
		cstmt.setString(2, customerID);
		cstmt.setString(3, shoppingCartID);
		cstmt.registerOutParameter(4, Types.FLOAT);
		cstmt.execute();
		
		return Double.parseDouble(df.format(cstmt.getFloat(4)));
	}
}

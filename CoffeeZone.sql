DROP TABLE shopping_order;
DROP TABLE orders;
DROP TABLE shopping_cart;
DROP TABLE menu;
DROP TABLE custom_quantity;
DROP TABLE customization;
DROP TABLE extras;
DROP TABLE products;
DROP TABLE customers;

/*Creates the customers table*/
CREATE TABLE customers(
    customer_id VARCHAR2(5),
    username VARCHAR2(50) UNIQUE,
    salt VARCHAR2(50),
    hash_password RAW(64),
    email VARCHAR2(75),
    phone VARCHAR2(20),
    referid VARCHAR2(5),
    freebies NUMBER(1,0),
    last_login TIMESTAMP,
    CONSTRAINT customer_id_pk PRIMARY KEY(customer_id),
    CONSTRAINT referid_fk FOREIGN KEY(referid)
        REFERENCES customers (customer_id)
        ON DELETE CASCADE
);

/*Creates the product table*/
CREATE TABLE products (
    product_id VARCHAR2(5),
    product_name VARCHAR2(50),
    retail NUMBER(8,2),
    quantity NUMBER(4,0),
    --custom_id VARCHAR2(5),
    CONSTRAINT products_pk PRIMARY KEY(product_id)
);

/*Creates extras table*/
CREATE TABLE extras(
    extra_id VARCHAR2(5),
    item_name VARCHAR2(50),
    price NUMBER(8,2),
    CONSTRAINT extra_id_pk PRIMARY KEY(extra_id)
);

/*Creates customization table*/
CREATE TABLE customization (
    --custom_id VARCHAR2(5),
    product_id VARCHAR2(5),
    extra_id VARCHAR2(5), 
    CONSTRAINT product_id_FK FOREIGN KEY(product_id)--custom_id)
        REFERENCES products (product_id),--custom_id),
    CONSTRAINT extra_id_fk FOREIGN KEY (extra_id)
        REFERENCES extras (extra_id)
        ON DELETE CASCADE
);

/*Create table custom_quantity*/
CREATE TABLE custom_quantity(
    custom_id VARCHAR2(5),
    extra_id VARCHAR2(5),
    quantity NUMBER(4),
    custom_price NUMBER(8,2),
    CONSTRAINT extra_id_cq_fk FOREIGN KEY (extra_id)
        REFERENCES extras (extra_id)
        ON DELETE CASCADE
);

/*Creates the menu table*/
CREATE TABLE menu(
    menu_id VARCHAR2(5),
    product_id VARCHAR2(5) NOT NULL,
    customer_id VARCHAR2(50) NOT NULL,
    custom_id VARCHAR2(6),
    price NUMBER(8,2),
    quantity NUMBER(4),
    CONSTRAINT customer_id_fk FOREIGN KEY(customer_id)
        REFERENCES customers (customer_id)
        ON DELETE CASCADE
);

/*Create table Shopping_cart*/
CREATE TABLE shopping_cart(
    shopping_cart_id VARCHAR2(5),
    menu_id VARCHAR2(5),
    menu_price NUMBER(8,2), 
    quantity NUMBER(4)
);

/*Create table orders*/
CREATE TABLE orders(
    order_id VARCHAR2(5),
    customer_id VARCHAR2(5),
    --shopping_cart_id VARCHAR2(5),
    total_quantity NUMBER(4),
    order_date DATE,
    delivered_date DATE,
    address VARCHAR2(50),
    CONSTRAINT order_id_pk PRIMARY KEY (order_id),
    /*CONSTRAINT shopping_cart_id_fk FOREIGN KEY (shopping_cart_id)
        REFERENCES shopping_cart (shopping_cart_id)
        ON DELETE CASCADE,*/
    CONSTRAINT customer_id_ofk FOREIGN KEY(customer_id)
        REFERENCES customers (customer_id)
);

/*Create Table Shopping_order*/
CREATE TABLE shopping_order(
    shopping_cart_id VARCHAR2(5),
    order_id VARCHAR2(5),
    CONSTRAINT shopping_order_pk PRIMARY KEY(shopping_cart_id,order_id),
    CONSTRAINT order_id_fk FOREIGN KEY (order_id)
        REFERENCES orders (order_id)
        ON DELETE CASCADE
);

/*Insert data in product table*/
INSERT INTO products
    VALUES('00001','Tea',2.25,25);
INSERT INTO products
    VALUES('00002','Coffee',2.10,50);
INSERT INTO products
    VALUES('00003','Orange Jus',3.05,15);
INSERT INTO products
    VALUES('00004','Chocolate Cupcake',3.75,5);
INSERT INTO products
    VALUES('00005','Strawberry Cupcake',3.99,5);
INSERT INTO products
    VALUES('00006','Vanilla Cupcake',3.50,5);
INSERT INTO products
    VALUES('00007','Cheese Cake',33.45,3);
INSERT INTO products
    VALUES('00008','Ice Cream Cake',42.88,4);
INSERT INTO products
    VALUES('00009','Croissant',3.45,10);
INSERT INTO products
    VALUES('00010','Baggel',2.99,13);
INSERT INTO products
    VALUES('00011','Toast',2.13,20);

/*Insert values in extra*/
INSERT INTO extras
    VALUES('00001','Milk',0.10);
INSERT INTO extras
    VALUES('00002','Sugar',0.05);
INSERT INTO extras
    VALUES('00003','Cream',0.10);
INSERT INTO extras
    VALUES('00004','Butter',0.10);
INSERT INTO extras
    VALUES('00005','Cream Cheese',0.25);
INSERT INTO extras
    VALUES('00006','Strawberry Jam',0.20);
INSERT INTO extras
    VALUES('00007','Peanut Butter',0.20);
    
/*Insert values in customization*/
INSERT INTO customization
    VALUES('00001','00001');
INSERT INTO customization
    VALUES('00001','00002');
INSERT INTO customization
    VALUES('00002','00001');
INSERT INTO customization
    VALUES('00002','00002');
INSERT INTO customization
    VALUES('00002','00003');
INSERT INTO customization
    VALUES('00010','00004');
INSERT INTO customization
    VALUES('00010','00005');
INSERT INTO customization
    VALUES('00010','00006');
INSERT INTO customization
    VALUES('00010','00007');
INSERT INTO customization
    VALUES('00011','00004');
INSERT INTO customization
    VALUES('00011','00005');
INSERT INTO customization
    VALUES('00011','00006');
INSERT INTO customization
    VALUES('00011','00007');
COMMIT;    
/*Quantity Update � Create a trigger that automatically updates the quantity of the product in stock when a product is ordered. 
    In the event that the quantity is less than 1, raise an exception with the message �This product is no longer in stock�.*/    
CREATE OR REPLACE TRIGGER before_menuUpdate_insert BEFORE
    INSERT ON menu
    FOR EACH ROW
DECLARE
    stockQuantity NUMBER(4,0);
    notEnoughQuantity EXCEPTION;
    PRAGMA EXCEPTION_INIT(notEnoughQuantity,-20000);
BEGIN 
    SELECT quantity INTO stockQuantity FROM products
    WHERE product_id LIKE :NEW.product_id;
    
    IF (:NEW.quantity>stockQuantity OR stockQuantity<0) THEN
        RAISE_APPLICATION_ERROR (-20000,'Not enough product');
    END IF;
END before_quantityUpdate_insert;
/

/*Trigger that looks if all there are enough stock for all the product in the order on insert to the shopping_order table*/
CREATE OR REPLACE TRIGGER before_shoppingOrderUpdate_insert BEFORE
    INSERT ON shopping_order
    FOR EACH ROW
DECLARE
    TYPE productIDArray IS VARRAY(100) OF VARCHAR2(5);
    TYPE menuIDArray IS VARRAY(100) OF VARCHAR2(5);
    TYPE menuQuantityArray IS VARRAY(100) OF NUMBER(3);
    TYPE productQuantityArray IS VARRAY(100) OF NUMBER(3);
    productIDList productIDArray := productIDArray();    
    menuIDList menuIDArray := menuIDArray();
    menuQuantityList menuQuantityArray := menuQuantityArray();
    productQuantityList productQuantityArray := productQuantityArray();
    
    cartQuantity NUMBER(3);
    menuQuantity NUMBER(3);
    productQuantity NUMBER(3);
    orderQuantity NUMBER(3);
    
    notEnoughQuantity EXCEPTION;
    PRAGMA EXCEPTION_INIT(notEnoughQuantity,-20000);
BEGIN
    productIDList.EXTEND(100);
    menuIDList.EXTEND(100);
    menuQuantityList.EXTEND(100);
    productQuantityList.EXTEND(100);
    
    SELECT menu_id,quantity BULK COLLECT INTO menuIDList,menuQuantityList FROM shopping_cart WHERE shopping_cart_id LIKE :NEW.shopping_cart_id;
    --For each menu in the shopping cart it loop throught it
    FOR i IN 1 .. menuIDList.COUNT LOOP
        cartQuantity := menuQuantityList(i);
        SELECT product_id,quantity BULK COLLECT INTO productIDList,productQuantityList FROM menu WHERE menu_id LIKE menuIDList(i);
        --For each product in the menu, it loops through it
        FOR i IN 1 .. productIDList.COUNT LOOP
            menuQuantity := productQuantityList(i);
            orderQuantity := menuQuantity*cartQuantity;
            SELECT quantity INTO productQuantity FROM products WHERE product_id LIKE productIDList(i);
            --If the product does not have enough quantity, it raise an error
            IF (orderQuantity>productQuantity) THEN
                RAISE_APPLICATION_ERROR (-20000,'Not enough product');
            ELSE
                 UPDATE products SET quantity = quantity-orderQuantity WHERE product_id = productIDList(i);
            END IF;
        END LOOP;
    END LOOP;
END;
/

/*Trigger on insert in customers table. If a customer referid was not null, it gives a freebie to the reference*/
CREATE OR REPLACE TRIGGER after_referUpdate_insert AFTER
    INSERT ON customers
    FOR EACH ROW
DECLARE
    referenceID customers.customer_ID%TYPE;
BEGIN
    IF (:OLD.referid IS NOT NULL) THEN
    SELECT customer_ID INTO referenceID FROM customers WHERE customer_ID LIKE :OLD.referid;
    UPDATE customers SET freebies = freebies+1 WHERE customer_id LIKE referenceID;
    END IF;
END;
/ 

/*Procedure that creates a shopping_order row*/
CREATE OR REPLACE PROCEDURE createShoppingOrder(shoppingCartID IN shopping_cart.shopping_cart_ID%TYPE, orderID IN orders.order_id%TYPE)
AS
BEGIN
    INSERT INTO shopping_order VALUES(shoppingCartID,orderID);
END;
/

/*Function that get the total of the order*/
CREATE OR REPLACE FUNCTION getOrderTotal(shoppingCartID IN shopping_cart.shopping_cart_ID%TYPE) RETURN NUMBER
AS
    total NUMBER(6,2);
BEGIN
    SELECT SUM(menu_price) INTO total FROM shopping_cart WHERE shopping_cart_id LIKE shoppingCartID;
    RETURN total;
END;
/
/*Function that get the customer ID based on his username*/
CREATE OR REPLACE FUNCTION getCustomerID(userN IN customers.username%TYPE) RETURN VARCHAR2
AS
    customerID VARCHAR(5);
BEGIN
    SELECT customer_id INTO customerID FROM customers WHERE username LIKE userN;
    RETURN customerID;
END;
/

/*Procedure that adds a new order*/
CREATE OR REPLACE PROCEDURE addOrder (orderID IN orders.order_id%TYPE, customerID orders.customer_id%TYPE, 
totalQuantity orders.total_quantity%TYPE,orderDate orders.order_date%TYPE, newAddress orders.address%TYPE)
AS
BEGIN
    INSERT INTO orders VALUES(orderID,customerID,totalQuantity,orderDate,null,newAddress);
END;
/

/*Function that gets the total of a menu*/
CREATE OR REPLACE FUNCTION getTotalMenu (shoppingCartID IN shopping_cart.shopping_cart_id%TYPE) RETURN NUMBER
AS
    totalMenu NUMBER(3,0);
BEGIN
    SELECT SUM(quantity) INTO totalMenu FROM shopping_cart WHERE shopping_cart_id LIKE shoppingCartID;
    RETURN totalMenu;
END;
/

/*Function that gets the number of freebies a customer has*/
CREATE OR REPLACE FUNCTION getFreebies(customerID IN customers.customer_id%TYPE) RETURN NUMBER
AS
    totalFreebies NUMBER(1);
BEGIN
    SELECT freebies INTO totalFreebies FROM customers WHERE customer_id LIKE customerID;
    RETURN totalFreebies;
END;
/

/*Procedure that updates the user amount of freebie when they use one and calculate the new total with the bonus*/
CREATE OR REPLACE PROCEDURE useFreebies(toUseFreebie IN customers.freebies%TYPE,customerID IN customers.customer_id%TYPE,shoppingCartID IN shopping_cart.shopping_cart_id%TYPE, total OUT NUMBER)
AS
    discount NUMBER(2);
BEGIN
    UPDATE customers SET freebies = freebies-toUseFreebie WHERE customer_id LIKE customerID;
    total := getOrderTotal(shoppingCartID);
    discount:=5*toUseFreebie;
    --If the discount is greater than the original total
    IF(discount>total) THEN
        total := 0;
    ELSE
        total := total-discount;
    END IF;
    RETURN;
END;
/
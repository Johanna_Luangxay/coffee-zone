package frontend;

import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.Scanner;
import backend.Ordering;
import backend.Registration;
import backend.Utilities;

/**
 * This class interacts with the user to make the orders.
 * @author Johanna Luangxay
 * @version 2020-12-19
 *
 */
public class CoffeeZone {
	private static Connection con;
	private static Registration r;
	private static Ordering o;
	private static Utilities u;
	private static Scanner sc  =  new Scanner(System.in);
	private static String current_user;
	private static String shoppingCartID;

	/**
	 * Main method that connects to the database then call a method that does the rest.
	 * @param args
	 * @throws SQLException
	 * @throws NoSuchAlgorithmException
	 */
	public static void main(String[] args) throws SQLException, NoSuchAlgorithmException 
	{
		con = getConnection(); 
		r = new Registration(con);
		o = new Ordering(con);
		u = new Utilities(con);
		
		if(con!=null) 
		{		
			System.out.println("You are successfully connected to the DB: "+con);
			System.out.println("\nWelcome to CoffeeZone!\n");
			displayWelcomeMenu();
		}
		else
		{
			System.out.println("Connection failed");
		}	
	}

	/**
	 * It gets the connection from the database (my personal local database)
	 * @return
	 * @throws SQLException
	 */
	private static Connection getConnection() throws SQLException 
	{
		Connection conn = null;
		String user = "temporary";
		String password = "Joe792002";
		conn = DriverManager.getConnection
				("jdbc:oracle:thin:@//localhost:1521/Database", user, password);
		return conn;
	}
	
	/**
	 * This method shows the option of the user that did not login
	 * @throws NoSuchAlgorithmException
	 * @throws SQLException
	 */
	private static void displayWelcomeMenu() throws NoSuchAlgorithmException, SQLException
	{
		int choice=0;
		while(choice != 8) 
		{
			System.out.println("Enter 1 if you want to register");
			System.out.println("Enter 2 to login");
			System.out.println("Enter 3 to consult the products");
			System.out.println("Enter 8 to exit");
			choice = Integer.parseInt(sc.nextLine());
			if(choice == 1 || choice == 2 || choice == 3 || choice == 8)
			{
				dispatchChoice(choice);
			}
			else
			{
				System.out.println("\nYou did not enter a valid choice. Please try again.\n");	
			}
		}
	}
	
	/**
	 * This method display the option for users that have login
	 * @throws NoSuchAlgorithmException
	 * @throws SQLException
	 */
	private static void displayLoginMenu() throws NoSuchAlgorithmException, SQLException
	{
		int choice=0;
		while(choice != 7 && choice != 6) 
		{
			System.out.println("Enter 3 to consult the products");
			System.out.println("Enter 4 to create a menu");
			System.out.println("Enter 5 to add menu the cart");
			System.out.println("Enter 7 to logout");
			choice = Integer.parseInt(sc.nextLine());
			if(choice == 3 || choice == 4 || choice == 5 || choice == 7)
			{
				dispatchChoice(choice);	
			}
			else
			{
				System.out.println("You did not enter a valid choice. Please try again.");
			}
		}

		
	}
	/**
	 * This method display option after users have added menu to the cart.
	 * @throws NoSuchAlgorithmException
	 * @throws SQLException
	 */
	private static void displayOrderMenu() throws NoSuchAlgorithmException, SQLException
	{
		int choice = 0;
		while (choice !=9)
		{
			System.out.println("Enter 6 to order");		
			System.out.println("Enter 9 to return to the menu");
			choice = Integer.parseInt(sc.nextLine());
			if(choice == 6 || choice == 9)
			{
				dispatchChoice(choice);
			}
			else
			{
				System.out.println("You did not enter a valid choice. Please try again.");
			}
		}
		
	}
	/**
	 * This method redirects the user's choice to other method do the correct action required.
	 * @param choice
	 * @throws NoSuchAlgorithmException
	 * @throws SQLException
	 */
	private static void dispatchChoice (int choice) throws NoSuchAlgorithmException, SQLException
	{
		switch(choice)
		{
		case 1:
			newUser();
			break;
		case 2:
			login();
			break;
		case 3:
			u.getProducts();
			break;
		case 4:
			createMenu();
			break;
		case 5:
			addShoppingCart();
			break;
		case 6:
			createOrder();
			break;
		case 7:
			System.out.println("\nSuccessful logout! Goodbye!\n");
			break;
		case 8:
			System.out.println("\nGoodBye, See you soon!");
			con.close();
			break;
		}
	}
	
	/**
	 * This Method creates a new user it acts the user information and validate them before calling a method to add it to the database
	 * @throws NoSuchAlgorithmException
	 * @throws SQLException
	 */
	private static void newUser() throws NoSuchAlgorithmException, SQLException
	{
		//For USERNAME
		System.out.println("Enter your desired username: ");
		String username = sc.nextLine().toLowerCase();
		username = r.validateUsername(username);
		//For EMAIL
		boolean validEmail = false;
		String email="";
		System.out.println("Enter your email address: ");
		while (!validEmail)
		{
			email = sc.nextLine().toLowerCase();
			validEmail= email.matches("^(.+)@(.+)\\.(.+)$");
			if(!validEmail)
			{
				System.out.println("The email you enter is invalid. Please enter a valid email: ");
			}
		}	
		//For PHONE number
		boolean validPhone = false;
		String phone = "";
		System.out.println("Enter the 10 digits of your phone number: ");
		while (!validPhone)
		{
			
			phone = sc.nextLine();
			validPhone= phone.matches("^\\d{10}$");
			if(!validPhone)
			{
				System.out.println("The phone number you enter is not valid. Please enter a valid number. Do not put any special character.");
			}
		}
		//For REFERENCE
		System.out.println("Were you refered by another client? Enter yes or no.");
		String wasRefered = sc.nextLine();
		
		String referid;
		if(wasRefered.toLowerCase().equals("yes"))
		{
			System.out.println("What was the customer id of your reference? ");
			referid = sc.nextLine();
			//Make sure the reference has not more than 3 referid
			referid = r.validateReferid(referid);
		}
		else
		{
			referid = "";
		}
		//For PASSWORD
		boolean correspond = false;
		while (!correspond)
		{
			System.out.println("Enter your desired password: "); 
			String password = sc.nextLine();
			
			System.out.println("Re-enter your desired password for confirmation: "); 
			String confirmedPW = sc.nextLine();
			
			if(password.equals(confirmedPW))
			{
				r.newUser(username,password,email,phone,referid);
				correspond = true;
			}
			else
			{
				System.out.println("Passwords do not correspond. Please enter new password!");
			}
		}
	}
	/**
	 * Make sure the user is register
	 * @throws NoSuchAlgorithmException
	 * @throws SQLException
	 */
	public static void login() throws NoSuchAlgorithmException, SQLException
	{	
		System.out.println("Enter your username: ");
		String username = sc.nextLine().toLowerCase();
		
		System.out.println("Enter your password: ");
		String password = sc.nextLine();

		//Allows user to go to the login menu if they are in the system
		if(r.login(username,password))
		{
			System.out.println("\nYou are in the system.");
			r.updateLogin(username);
			current_user = username;
			displayLoginMenu();
		}
		else
		{
			System.out.println("\nWrong user or password. Try again");
			displayWelcomeMenu();
		}
	}

	/**
	 * Asks user question to create a menu then calls another method to put the menu in the database
	 * @throws SQLException
	 * @throws NoSuchAlgorithmException
	 */
	public static void createMenu() throws SQLException, NoSuchAlgorithmException
	{
		String wantProduct = "yes";
		String menuQuery = "SELECT COUNT(COUNT(menu_id)) FROM menu GROUP BY menu_id";
		//User need the menu ID to add to the cart after
		String menuID = u.getNewID(menuQuery);
		System.out.println("\nYour menu id is the following: "+menuID+"\n*Please remember it!\n");
		
		//Continue to add menu until user don't want to add more product
		while(wantProduct.equals("yes"))
		{
			System.out.println("Enter the item number of the desired item: ");
			String product_id = sc.nextLine();
			
			//Look if the product allows extra and if yes, the method display the extras
			int possibleExtra = u.getExtras(product_id);
			
			String customID = "";
			//If extra is possible
			if(possibleExtra != 0)
			{
				System.out.println("Do you want to add any extra? Enter yes or no.");
				String wantExtra = sc.nextLine().toLowerCase();
					
				if(wantExtra.equals("yes"))
				{	
					String customQuery = "SELECT COUNT(menu_id) FROM menu WHERE custom_id IS NOT NULL";
					//Methods look through the customID and gives the next customID available
					customID = u.getNewID(customQuery);
					//Add extras until user don't want extra anymore
					while(wantExtra.equals("yes"))
					{
					System.out.println("Enter the item number of the desired extra: ");
					String extra_id = sc.nextLine();
					
					System.out.println("Enter the wanted quantity for this extra: ");
					int extraQuantity = Integer.parseInt(sc.nextLine());
					
					o.createCustomQuantity(customID, extra_id, extraQuantity);
					
					System.out.println("Do you want to add anymore extra? Enter yes or no.");
					wantExtra = sc.nextLine().toLowerCase();
					}
				}				
			}
			//For quantity of the product needed (with the customization if added)
			int menuQuantity = 0;
			System.out.println("How many times would you like this (customized) product?");
			menuQuantity = Integer.parseInt(sc.nextLine());
			//Call the function that adds the menu to the database
			o.createMenu(menuID,product_id,current_user,customID,menuQuantity);
			
			System.out.println("Do you want to add more products? Enter yes or no.");
			wantProduct = sc.nextLine().toLowerCase();
		}
		//Gives the price of the menu
		String totalQuery = "SELECT SUM(price) FROM menu WHERE menu_id LIKE ?";
		System.out.println("\nTotal of the menu "+menuID+": "+u.getNumber(totalQuery,menuID)+"$\n");
	}

	/**
	 * Creates the shopping cart, asks user question to then call a function that adds the cart to the database
	 * @throws SQLException
	 * @throws NoSuchAlgorithmException
	 */
	private static void addShoppingCart() throws SQLException, NoSuchAlgorithmException 
	{
		String wantMenu = "yes";
		//Gets the next available id for the shopping cart
		String cartIDQuery = "SELECT COUNT(COUNT(shopping_cart_id)) FROM shopping_cart GROUP BY shopping_cart_id";
		shoppingCartID = u.getNewID(cartIDQuery);
		//Adds to the shopping cart as long as the user wants to keep adding menu
		while(wantMenu.equals("yes"))
		{
			//Gets the menu ID
			System.out.println("Enter the menu number you want to add: ");
			String menuID = sc.nextLine();
			//Quantity need for that menu
			System.out.println("How many times would you like this menu?");
			int quantity = Integer.parseInt(sc.nextLine());
			//Adds it to the database		
			o.createShoppingCart(shoppingCartID,menuID,quantity);
			con.commit();
			
			System.out.println("Do you want to add more menus? Enter yes or no.");
			wantMenu = sc.nextLine().toLowerCase();
		}
		
		//Gets the total for the shopping cart
		double totalShoppingCart= u.getOrderTotal(shoppingCartID);
		System.out.println("\nTotal of the shopping cart: "+totalShoppingCart+"$\n");
		
		displayOrderMenu();
	}

	/**
	 * Ask user info to create an order and deals with whether or not the user have discount/freebies/bonus.
	 * If the creation failed, it will cancel the use of freebies or anything added to the order table 
	 * @throws SQLException
	 * @throws NoSuchAlgorithmException
	 */
	private static void createOrder() throws SQLException, NoSuchAlgorithmException
	{
		//Gets the customer id form their username
		String customerID = u.getCustomerID(current_user);
		//Assigns the total of the order which is the shopping cart
		double total = u.getOrderTotal(shoppingCartID);
		String address = "";
		String answer = "no";	
		//Let's the user change the address if they made a mistake
		while(answer.equals("no"))
		{
			System.out.println("Enter your delivery adress: ");
			address = sc.nextLine();
			System.out.println("Is: "+address+"\nthe right address you want us to ship your order to? Enter yes or no.");
			answer = sc.nextLine().toLowerCase();
		}
				
		try
		{
			con.setAutoCommit(false);
			//Determines whether the customer have bonus/discount on their order
			int freebies = u.getFreebies(customerID);
			//If they have bonus, they can choose to use it and how many of them they want to use
			if(freebies>0)
			{
				System.out.println("You currently have "+freebies+" bonus that you can use on your order. 1 bonus reduces 5$ to a menu."
						+ "\nWould you like to use your bonus? Enter yes or no.");
				String useFreebies = sc.nextLine().toLowerCase();
				if(useFreebies.equals("yes"))
				{
					System.out.println("How many bonus would you like to use?");
					int bonus = Integer.parseInt(sc.nextLine());
					//Does not let user enter a number of bonus that they don't have
					while(bonus>freebies || bonus<0)
					{
						System.out.println("You enter a valid number. Please re-enter the number of bonus you would like to use");
						bonus = Integer.parseInt(sc.nextLine());
					}
					//Gets the new total if the bonus are used
					total = o.useFreebies(bonus,customerID,shoppingCartID);
				}
				else
				{
					System.out.println("Very well. You can always choose to use your bonus for a next time.\n");
				}
			}
			//Gives the final total
			System.out.println("\nTotal of the order: "+total+"$\n");
			//Makes the order
			o.createOrder(customerID,address,shoppingCartID);
			con.commit();
		}
		catch (SQLException e)
		{
			con.rollback();
			//If there isn't enough quantity in the end
			int errorCode = e.getErrorCode();
			if(errorCode == 20000)
			{
				System.out.println("\nThe ordered failed. Not enough product for the whole order.\n");
			}
			else
			{
				System.out.println("\nMenu creation failed for the following reason: "+e+"\n");	
			}
		}		
	}
}


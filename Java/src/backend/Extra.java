package backend;

/**
 * Class object of a extra
 * @author Johanna Luangxay
 * @version 2020-12-20
 *
 */
public class Extra {
	private String extra_id;
	private String extra_name;
	private double price;
	
	/**
	 * Constructor
	 * @param extra_id
	 * @param extra_name
	 * @param price
	 */
	public Extra(String extra_id,String extra_name, double price)
	{
		this.extra_id = extra_id;
		this.extra_name = extra_name;
		this.price = price;
	}
	
	/**
	 * Gives the info of an extra
	 */
	@Override
	public String toString()
	{
		return extra_id+" "+extra_name+" : "+price+"$";
	}
}

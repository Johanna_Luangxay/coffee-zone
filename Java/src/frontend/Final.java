package frontend;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Final {
	
	private static Connection con;
	private static Scanner sc;

	public static void main(String[] args) throws SQLException, NoSuchAlgorithmException 
	{
		con = getConnection(); 
		sc = new Scanner(System.in);
		
		if(con!=null) 
		{		
			System.out.println("You are successfully connected to the DB: "+con);
			showVisit();
			showVetSchedule();
		}
		else
		{
			System.out.println("Connection failed");
		}	
	}
	
	private static Connection getConnection() throws SQLException 
	{
		Connection conn = null;
		String user = "clinic";
		String password = "admin123";
		conn = DriverManager.getConnection
				("jdbc:oracle:thin:@123.456.78.90:1522/vet.ca", user, password);
		return conn;
	}
	
	private static void showVisit() throws SQLException
	{
		System.out.println("Here are the visits for the day: ");
		String todayVisitQuery = "SELECT visit_id, pet_id, vet_id, \"date\", description FROM visit WHERE date LIKE CURRENT_DATE"; 
		PreparedStatement ps = con.prepareStatement(todayVisitQuery);
		ResultSet rps = ps.executeQuery();
		while(rps.next()) 
		{
			System.out.println("Visit#: "+rps.getString(1)+" Pet ID: "+rps.getString(2)+"Vet ID: "+rps.getString(3)+" Visit Date: "
					+rps.getDate(4)+" Description: "+rps.getString(5));		
						
		}
	}
	
	private static void showVetSchedule() throws SQLException
	{
		System.out.println("Enter the vet name: ");
		String vetName = sc.nextLine(); 
		String todayVisitQuery = "SELECT pet_id,owner_id,date FROM visit JOIN pet USING (pet_id) JOIN veterinarian USING (vet_id) WHERE name LIKE ? AND date>=CURRENT_DATE ORDER BY date ASC"; 
		PreparedStatement ps = con.prepareStatement(todayVisitQuery);
		ps.setString(1, vetName);
		ResultSet rps = ps.executeQuery();
		System.out.println("Here are the next visit for "+vetName+": ");
		while(rps.next())
		{
			System.out.println("Owner ID: "+rps.getString(1)+" Pet ID: "+rps.getString(2)+" Date of Visit: "+rps.getDate(3));
		}
	}
	
}

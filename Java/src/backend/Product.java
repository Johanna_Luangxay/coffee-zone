package backend;

/**
 * Class object representing a product
 * @author Johanna Luangxay
 * @version 2020-12-20
 *
 */
public class Product 
{
	private String product_id;
	private String productName;
	private double retail;
	private int quantity;
	
	/**
	 * Constructor
	 * @param product_id
	 * @param productName
	 * @param retail
	 * @param quantity
	 */
	public Product(String product_id,String productName, double retail,int quantity)
	{
		this.product_id = product_id;
		this.productName = productName;
		this.retail = retail;
		this.quantity = quantity;
	}
	
	/**
	 * Gives information of the product
	 */
	@Override
	public String toString()
	{
		return product_id+" "+productName+" : "+retail+"$ - In stock quantity: "+quantity;
	}
}

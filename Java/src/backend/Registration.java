package backend;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.nio.charset.StandardCharsets;

/**
 * Class that takes care of everything related to login and registration with the DB
 * @author Johanna Luangxay
 * @version 2020-12-20
 *
 */
public class Registration {
	Connection con;
	
	private static Scanner sc  =  new Scanner(System.in);
	
	/**
	 * Constructor that takes in the database connection
	 * @param con
	 */
	public Registration(Connection con)
	{
		this.con = con;
	}
	
	/**
	 * Adds the a new user to the DB but salts and hash the passwords before
	 * @param username
	 * @param password
	 * @param email
	 * @param phone
	 * @param referid
	 * @throws NoSuchAlgorithmException
	 * @throws SQLException
	 */
	public void newUser(String username, String password, String email, String phone, String referid) throws NoSuchAlgorithmException, SQLException
	{
		try
		{			
			con.setAutoCommit(false);
			//Makes the salt and hashes it
			String salt = getSalt();
			byte[] hashedPW = hash(password,salt);
			String customerID = getCustomerID();
			//Inserts it in the DB
			String userQuery = "INSERT INTO customers VALUES(?,?,?,?,?,?,?,?,?)";
			PreparedStatement ups = con.prepareStatement(userQuery);
			
			ups.setString(1,customerID);
			ups.setString(2,username);
			ups.setString(3,salt);
			ups.setBytes(4,hashedPW);
			ups.setString(5,email);
			ups.setString(6,phone);
			ups.setString(7,referid);
			ups.setInt(8,0);
			ups.setString(9,"");

			ups.executeUpdate();
			System.out.println("You are now in the system. Now, please login to start ordering.\n");
			
			con.commit();
		}
		catch(SQLException e)
		{
			System.out.println("\nUser creation failed. For the following reason: "+e+"\n");
			con.rollback();
		}
	}
	
	/**
	 * Finds the next available customer id
	 * @return String that represents the customer ID
	 * @throws SQLException
	 */
	private String getCustomerID() throws SQLException
	{
		String customerID = "";
		int totalCustomer = 0;
		String totalCustomerQuery = "SELECT COUNT(*) FROM customers";
		PreparedStatement tcps = con.prepareStatement(totalCustomerQuery);
		ResultSet rtcps = tcps.executeQuery();
		
		while(rtcps.next()) 
		{
			totalCustomer = Integer.parseInt((String) rtcps.getString(1));
		}

		int ALLOWED_CHAR = 5;

		int NUMBER_ADDED_CUSTOMERS = 1;
		totalCustomer =totalCustomer+NUMBER_ADDED_CUSTOMERS;
				
		customerID = String.format("%0"+ALLOWED_CHAR+"d", totalCustomer);
		
		System.out.println("\nYour customer id is the following: "+customerID+"\n*Please remember it!\n");
		
		return customerID;
	}
	
	/**
	 * Determines whether the username and password correspond in the DB
	 * @param username
	 * @param password
	 * @return boolean of whether or not the users figures in the DB
	 * @throws SQLException
	 * @throws NoSuchAlgorithmException
	 */
	public boolean login(String username, String password) throws SQLException, NoSuchAlgorithmException
	{ 
		try
		{
			String userQuery = "SELECT salt, hash_password FROM customers WHERE username LIKE ?";
			PreparedStatement ups = con.prepareStatement(userQuery);
			ups.setString(1,username);
			
			ResultSet rups = ups.executeQuery();
			
			String salt = "";
			byte[] hash = {};
			
			while(rups.next()) 
			{
				salt = rups.getString(1);
				hash = rups.getBytes(2);
			}
			
			byte[] hashedPW = hash(password,salt);

			//If there is one index that is not the same, the password is not the same
			for(int i = 0; i<hashedPW.length; i++)
			{
				if (hashedPW[i] != hash[i])
				{
					return false;
				}
			}
			return true;
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			return false;
		}
	}
	
	/**
	 * Everytime the users is login, this method update the customer table so that it corresponds to the last time the user logged in
	 * @param username
	 * @throws SQLException
	 */
	public void updateLogin(String username) throws SQLException
	{
		String loginQuery = "UPDATE customers SET last_login=TO_CHAR(CURRENT_TIMESTAMP, 'YYYY-MM-DD HH24:MI:SS') WHERE username LIKE ?";
		PreparedStatement lps = con.prepareStatement(loginQuery);
		lps.setString(1,username);
		lps.executeUpdate();
	}
	
	/**
	 * Gives the salt
	 * @return String representing the salt
	 */
	private String getSalt()
	{
		SecureRandom random = new SecureRandom();
		String salt = new BigInteger(130,random).toString(32);
		return salt;
	}
	
	/**
	 * Hashes the code
	 * @param password
	 * @param salt
	 * @return byte[] that represents the hashed code
	 * @throws NoSuchAlgorithmException
	 */
	private byte[] hash(String password, String salt) throws NoSuchAlgorithmException
	{
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		String stringToHash = password+salt;
		return md.digest(stringToHash.getBytes(StandardCharsets.UTF_8));
	}
	
	/**
	 * Check if the reference can get another bonus/freebies
	 * @param referid
	 * @return
	 * @throws NumberFormatException
	 * @throws SQLException
	 */
	public String validateReferid(String referid) throws NumberFormatException, SQLException
	{
		//Get the number of customer that the reference referred to
		String referQuery = "SELECT COUNT(customer_id) FROM customers WHERE referid LIKE ?";
		PreparedStatement rps = con.prepareStatement(referQuery);
		rps.setString(1,referid);
		ResultSet rrps = rps.executeQuery();
		
		int totalCustomer = 0;
		while(rrps.next()) 
		{
			totalCustomer = Integer.parseInt((String) rrps.getString(1));
		}
		
		//They can only refer to max 3 people
		final int MAX_REFERED = 3;
		if(totalCustomer>=MAX_REFERED)
		{
			System.out.println("\nThe customer who refered has already reached maximum of 3 references.\n");
			referid = "";
		}
		else
		{
			//They get another freebie if they didn't refer to 3 people yet
			referQuery = "UPDATE customers SET freebies = freebies + 1 WHERE customer_id LIKE ?";
			rps = con.prepareStatement(referQuery);
			rps.setString(1, referid);
			rrps = rps.executeQuery();
		}

		return referid;
	}

	/**
	 * Method that does not allow user to enter a already taken username
	 * @param username
	 * @return String of the unique username chosen by the user
	 * @throws SQLException
	 */
	public String validateUsername(String username) throws SQLException 
	{
		boolean isSame = false;
		//Loops through until user enters a unique username
		while(!isSame)
		{
			String usernameQuery = "SELECT COUNT(customer_id) FROM customers WHERE username LIKE ?";
			PreparedStatement ups = con.prepareStatement(usernameQuery);
			ups.setString(1,username);
			ResultSet rups = ups.executeQuery();
			
			int totalCustomer = 0;
			while(rups.next()) 
			{
				totalCustomer = Integer.parseInt((String) rups.getString(1));
			}
			
			if (totalCustomer>0)
			{
				System.out.println("This username is already taken. Please choose another one.");
				username = sc.nextLine();
			}
			else
			{
				isSame = true;
			}
		}
		return username;
	}

}
